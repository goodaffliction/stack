﻿#include <iostream>
#include <stack>

class Stack 
{
public:
	Stack *next;		//ukazatel
	int size;		//kolichestvo elementov v steke
};

void s_push(Stack **top, int D) // esli *top - eto vershina steka, to chto takoe **top? ukazatel na vershinu?
{
	Stack* q;          // sozdaem ssylku q tipa Stack
	int x;             // ob'yavlyaem peremennuu x tipa int
	std::cin >> x;     
	q = new Stack[x];  // pytaemsya sozdat' dinamicheskii massiv
	q->size = D;       // razimenovyvaem size iz classa Stack i prisvaivaem znachenie D
	if (top == NULL)   //dalee proveryaem esli top ravno 0
	{
		*top = q;      // to adresu top prisvaivaetsya znachenie q
	}
	else
	{
		q->next = *top; // inache q razimenovyvaet next iz classa Stack
		*top = q;
	}
}
void s_print(Stack* top) 
{
	Stack* q = top; 
	while (q) 
	{ 
		printf_s("%i", q->size); 
		q = q->next;
	}
}
int main()
{
	Stack* top = NULL;
	s_push(&top, 1);
	s_push(&top, 2);
	s_push(&top, 3);
	s_push(&top, 4);
	s_push(&top, 5);
	s_push(&top, 7);
	s_push(&top, 8);
	//после выполнения функций в стеке у нас будет 54321
	s_print(top);//выводим
	//s_delete_key(&top, 4); //Затем удаляем 4, в стеке получается 5321
	printf_s("\n");//переводим на новую строку
	s_print(top);//выводим
	system("pause");//ставим на паузу
}
